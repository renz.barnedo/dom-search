import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
})
export class ItemComponent implements OnInit {
  @Input() domain = '';
  @Input() showAddToScratchboxButton: boolean = false;
  @Input() showRegisterButton: boolean = false;
  @Output() triggerAddToScratchboard = new EventEmitter();
  constructor(private uiService: UiService) {}

  ngOnInit(): void {}

  addToScratchboard() {
    this.uiService.openSnackbar(`Added ${this.domain} to scratchboard...`);
    this.triggerAddToScratchboard.emit(this.domain);
  }

  register() {
    const url = 'https://swiy.io/Interserver';
    window.open(url, '_blank');
  }
}
