import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css'],
})
export class ResultsComponent implements OnInit {
  constructor() {}

  private resultsSubscription!: Subscription;
  @Input() results!: Observable<void>;
  @Output() saveToScratchboard: EventEmitter<string> = new EventEmitter();
  domains: any;
  availableIsOpened = true;

  ngOnInit(): void {
    if (!this.results) {
      return;
    }
    this.resultsSubscription = this.results.subscribe(
      (value) => (this.domains = value)
    );
  }

  ngOnDestroy(): void {
    this.resultsSubscription.unsubscribe();
  }

  addToScratchboard(domain: string) {
    this.saveToScratchboard.next(domain);
  }
}
