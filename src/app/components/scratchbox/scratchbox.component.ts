import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { Observable, Subscription } from 'rxjs';
import { CheckerService } from 'src/app/services/checker.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-scratchbox',
  templateUrl: './scratchbox.component.html',
  styleUrls: ['./scratchbox.component.css'],
})
export class ScratchboxComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private dbService: NgxIndexedDBService,
    private uiService: UiService,
    private checkerService: CheckerService
  ) {}

  private domainsSubscription!: Subscription;
  @Input() domains!: Observable<void>;
  @Output() showScratchboxResults = new EventEmitter();
  savedDomains: string[] = [];
  scratchboardForm: FormGroup = this.fb.group({});

  async ngOnInit() {
    await this.setScratchboardFormControls();
    if (!this.domains) {
      return;
    }
    this.domainsSubscription = this.domains.subscribe((value: any) => {
      if (!this.savedDomains.includes(value)) {
        this.savedDomains.push(value);
        const stringedDomains = this.savedDomains
          .map((name: string) => name + '\n')
          .join('');
        this.scratchboardForm.get('domains')?.setValue(stringedDomains);
        this.updateSavedDomains();
      }
    });
  }

  showForm = false;
  async setScratchboardFormControls() {
    const savedDomains: any = await this.getSavedDomains();
    if (!savedDomains || !savedDomains.length) {
      await this.dbService.add('domains', {
        domain: '',
      });
    }
    let domains = savedDomains || '';
    domains = domains
      .map((domain: { domain: string; id: number }) => domain.domain)
      .join('\n');
    this.scratchboardForm = this.fb.group({
      domains: this.fb.control(domains, Validators.required),
    });
    this.savedDomains = domains.split('\n').filter((domain: string) => domain);
    this.showForm = true;
  }

  ngOnDestroy(): void {
    this.domainsSubscription.unsubscribe();
  }

  async checkAvailability() {
    if (this.scratchboardForm.status !== 'VALID') {
      this.uiService.openDialog('Error', 'Please fix your fields.');
      return;
    }
    this.uiService.openSpinner();
    let domains = this.scratchboardForm.value.domains
      .split('\n')
      .filter((domain: string) => domain);
    try {
      const response: any = await this.checkerService.check(domains);
      if (response.status !== 200) {
        this.uiService.closeSpinner();
        this.uiService.openDialog(
          'Error',
          'Please make sure there is a TLD (.com, .net, etc) for each domain on your scratchboard'
        );
        return;
      }
      domains = response.data.domains.map((domain: any) => ({
        domain: domain.domain,
        available: domain.available,
      }));
      this.showScratchboxResults.emit(domains);
    } catch (error) {
      this.uiService.openDialog('Error', 'Cannot check, please try again.');
    }
    this.uiService.closeSpinner();
  }

  clearScratchboard() {
    this.scratchboardForm.reset();
  }

  async getSavedDomains() {
    try {
      const domains = await this.dbService.getAll('domains');
      return await new Promise((resolve, reject) =>
        domains.subscribe(
          (result: any) => resolve(result),
          (error: any) => reject(error)
        )
      );
    } catch (error) {
      this.uiService.openDialog('Error', 'Cannot get saved domains.');
    }
  }

  async updateSavedDomains() {
    const domains = this.scratchboardForm.value.domains;
    try {
      await this.dbService.update('domains', {
        id: 1,
        domain: domains,
      });
    } catch (error) {
      console.error('Cannot update saved domains', error);
    }
  }
}
