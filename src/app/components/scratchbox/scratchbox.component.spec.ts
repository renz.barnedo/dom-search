import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScratchboxComponent } from './scratchbox.component';

describe('ScratchboxComponent', () => {
  let component: ScratchboxComponent;
  let fixture: ComponentFixture<ScratchboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScratchboxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScratchboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
