import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css'],
})
export class ListItemComponent implements OnInit {
  @Input() domains: string[] = [];
  @Input() subheader = '';
  @Output() triggerAddToScratchboard = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  addToScratchboard(domain: string) {
    this.triggerAddToScratchboard.emit(domain);
  }
}
