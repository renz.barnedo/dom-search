import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CheckerService } from 'src/app/services/checker.service';
import { UiService } from 'src/app/services/ui.service';
import wordlist from '../../../assets/data/wordlist.json';

@Component({
  selector: 'app-word-forms',
  templateUrl: './word-forms.component.html',
  styleUrls: ['./word-forms.component.css'],
})
export class WordFormsComponent implements OnInit {
  @Input() tlds: string[] = [];
  @Output() showResults = new EventEmitter();
  wordForm: FormGroup = this.fb.group({});

  constructor(
    private fb: FormBuilder,
    private uiService: UiService,
    private checkerService: CheckerService
  ) {}

  ngOnInit(): void {
    this.setWordForm();
  }

  wordlist: any = wordlist;
  leftnames = this.filterNames(wordlist.leftnames);
  rightnames = this.filterNames(wordlist.rightnames);

  setWordForm() {
    this.wordForm = this.fb.group({
      left: this.fb.group({
        chosenName: this.fb.control(''),
        keywords: this.fb.control('', Validators.required),
        names: wordlist.leftnames,
      }),
      right: this.fb.group({
        chosenName: this.fb.control(''),
        keywords: this.fb.control('', Validators.required),
        names: wordlist.rightnames,
      }),
      topLevelDomainSelection: this.fb.control('com', Validators.required),
      topLevelDomainInput: this.fb.control(''),
    });
    // for testing purposes
    this.onUserPick('left');
    this.onUserPick('right');
  }

  filterNames(names: any) {
    return Object.keys(names).filter((name: string) => name !== 'main');
  }

  clearBox(box: string) {
    const chosenBox: any = this.wordForm.get(box);
    chosenBox.controls['keywords'].setValue('');
    chosenBox.controls['chosenName'].setValue('');
  }

  onUserPick(box: string) {
    const boxValue = this.wordForm.get(box)?.value;
    const chosenName = boxValue.chosenName;
    const keywords = boxValue.names[chosenName]
      .map((name: string) => name + '\n')
      .join('');
    const chosenBox: any = this.wordForm.get(box);
    chosenBox.controls['keywords'].setValue(keywords);
  }

  checkTld(tld: string) {
    if (!tld) {
      return '';
    }
    return `.${tld.replace(/[^0-9a-z-A-Z]/g, '')}`;
  }

  combineDomains() {
    const values = this.wordForm.value;
    const leftKeywords = values.left.keywords.split('\n');
    const rightKeywords = values.right.keywords.split('\n');
    const domains: string[] = [];
    const tld =
      this.checkTld(values.topLevelDomainInput) ||
      '.' + values.topLevelDomainSelection;
    leftKeywords.forEach((leftKeyword: string) =>
      rightKeywords.forEach((rightKeyword: string) => {
        if (!leftKeyword || !rightKeyword) {
          return;
        }
        const domain = `${leftKeyword}${rightKeyword}${tld}`.toLowerCase();
        domains.push(domain);
      })
    );
    return domains;
  }

  async onWordFormSubmit() {
    if (this.wordForm.status !== 'VALID') {
      this.uiService.openDialog('Error', 'Please fix your fields.');
      return;
    }
    this.uiService.openSpinner();
    let domains: any = this.combineDomains();
    try {
      const response: any = await this.checkerService.check(domains);
      if (response.status !== 200) {
        this.uiService.closeSpinner();
        this.uiService.openDialog('Error', 'Please try another TLD.');
        return;
      }
      domains = this.checkerService.categorize(response.data.domains);
      this.showResults.emit(domains);
    } catch (error) {
      this.uiService.openDialog('Error', 'Cannot check, try again.');
    }
    this.uiService.closeSpinner();
  }
}
