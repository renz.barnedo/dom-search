import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WordFormsComponent } from './word-forms.component';

describe('WordFormsComponent', () => {
  let component: WordFormsComponent;
  let fixture: ComponentFixture<WordFormsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WordFormsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WordFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
