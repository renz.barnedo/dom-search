import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsScratchboxComponent } from './results-scratchbox.component';

describe('ResultsScratchboxComponent', () => {
  let component: ResultsScratchboxComponent;
  let fixture: ComponentFixture<ResultsScratchboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultsScratchboxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsScratchboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
