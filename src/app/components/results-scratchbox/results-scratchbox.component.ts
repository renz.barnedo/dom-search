import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-results-scratchbox',
  templateUrl: './results-scratchbox.component.html',
  styleUrls: ['./results-scratchbox.component.css'],
})
export class ResultsScratchboxComponent implements OnInit {
  constructor(private uiService: UiService) {}

  private resultsSubscription!: Subscription;
  @Input() results!: Observable<void>;
  @Output() saveToScratchboard: EventEmitter<string> = new EventEmitter();
  domains: any;

  ngOnInit(): void {
    if (!this.results) {
      return;
    }
    this.resultsSubscription = this.results.subscribe(
      (value) => (this.domains = value)
    );
  }

  ngOnDestroy(): void {
    this.resultsSubscription.unsubscribe();
  }

  addToScratchboard(domain: string) {
    this.saveToScratchboard.next(domain);
  }

  setDisplayMessage(domain: { domain: string; available: boolean }) {
    return this.uiService.setDisplayMessage(domain);
  }
}
