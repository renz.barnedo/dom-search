import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CombineButtonComponent } from './combine-button.component';

describe('CombineButtonComponent', () => {
  let component: CombineButtonComponent;
  let fixture: ComponentFixture<CombineButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CombineButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CombineButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
