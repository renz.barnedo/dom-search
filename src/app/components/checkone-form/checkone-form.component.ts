import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CheckerService } from 'src/app/services/checker.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-checkone-form',
  templateUrl: './checkone-form.component.html',
  styleUrls: ['./checkone-form.component.css'],
})
export class CheckoneFormComponent implements OnInit {
  @Input() tlds: string[] = [];

  constructor(
    private fb: FormBuilder,
    private checkerService: CheckerService,
    private uiService: UiService
  ) {}

  ngOnInit(): void {
    this.setCheckDomainFormControls();
  }

  checkDomainForm: FormGroup = this.fb.group({});

  setCheckDomainFormControls() {
    this.checkDomainForm = this.fb.group({
      domain: this.fb.control('', Validators.required),
      topLevelDomain: this.fb.control('com', Validators.required),
    });
  }

  response: any;
  async checkDomain() {
    if (this.checkDomainForm.status !== 'VALID') {
      this.uiService.openDialog(
        'Error',
        'Please enter a domain name (Ex: mybusinessname.com)'
      );
      return;
    }
    this.uiService.openSpinner();
    const value = this.checkDomainForm.value;
    const domain = value.domain;
    const tld = value.topLevelDomain;
    const fullDomain = domain.includes('.') ? domain : `${domain}.${tld}`;
    try {
      const response: any = await this.checkerService.check([fullDomain]);
      if (response.status !== 200) {
        this.uiService.closeSpinner();
        this.response = null;
        this.uiService.openDialog(
          'Error',
          `Cannot check ${fullDomain} sorry, try again another TLD.`
        );
        return;
      }
      this.response = response?.data?.domains[0];
    } catch (error) {
      this.uiService.openDialog('Error', 'Cannot check, try again.');
    }
    this.uiService.closeSpinner();
  }

  setDisplayMessage(domain: { domain: string; available: boolean }) {
    return this.uiService.setDisplayMessage(domain);
  }
}
