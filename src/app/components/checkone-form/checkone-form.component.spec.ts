import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoneFormComponent } from './checkone-form.component';

describe('CheckoneFormComponent', () => {
  let component: CheckoneFormComponent;
  let fixture: ComponentFixture<CheckoneFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckoneFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoneFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
