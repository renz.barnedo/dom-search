import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import tlds from '../assets/data/tld.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  tlds: string[] = [];
  ngOnInit(): void {
    this.tlds = Object.keys(tlds);
  }

  resultsSubject: Subject<void> = new Subject<void>();
  domainsSubject: Subject<void> = new Subject<void>();
  scratchboxResultsSubject: Subject<void> = new Subject<void>();

  showResults(results: any) {
    this.resultsSubject.next(results);
  }

  showScratchboxResults(results: any) {
    this.scratchboxResultsSubject.next(results);
  }

  saveToScratchboard(domain: any) {
    this.domainsSubject.next(domain);
  }
}
