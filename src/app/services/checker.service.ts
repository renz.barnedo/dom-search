import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CheckerService {
  constructor(private http: HttpClient) {}

  private API_URL = environment.apiUrl;

  check(domains: string[]) {
    const fullApiUrl = this.API_URL + '/check';
    const promise = new Promise((resolve, reject) =>
      this.http.post(fullApiUrl, { domains }).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      )
    );
    return promise;
  }

  categorize(domains: any[]) {
    const available: string[] = [];
    const taken: string[] = [];
    domains.forEach((domain: any) =>
      domain.available
        ? available.push(domain.domain)
        : taken.push(domain.domain)
    );
    return {
      available,
      taken,
    };
  }
}
