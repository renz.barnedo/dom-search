import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarComponent } from '../components/snackbar/snackbar.component';
import { DialogComponent } from '../utilities/dialog/dialog.component';
import { SpinnerComponent } from '../utilities/spinner/spinner.component';

@Injectable({
  providedIn: 'root',
})
export class UiService {
  constructor(private dialog: MatDialog, private snackbar: MatSnackBar) {}

  openSpinner() {
    this.dialog.open(SpinnerComponent, {
      disableClose: true,
      panelClass: 'transparent',
    });
  }

  closeSpinner() {
    this.dialog.closeAll();
  }

  openDialog(title: string, message: string) {
    this.dialog.open(DialogComponent, {
      data: { title, message },
    });
  }

  openSnackbar(message: string) {
    this.snackbar.openFromComponent(SnackbarComponent, {
      duration: 3000,
      data: { message },
    });
  }

  setDisplayMessage(domain: { domain: string; available: boolean }) {
    return `${domain.domain} is ${
      domain.available ? 'available!' : 'already taken.'
    }`;
  }
}
