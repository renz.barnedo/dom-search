import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { MaterialModule } from './themes/material/material.module';
import { HintComponent } from './components/hint/hint.component';
import { WordFormsComponent } from './components/word-forms/word-forms.component';
import { HelpTextComponent } from './components/help-text/help-text.component';
import { CombineButtonComponent } from './components/combine-button/combine-button.component';
import { ScratchboxComponent } from './components/scratchbox/scratchbox.component';
import { CheckoneFormComponent } from './components/checkone-form/checkone-form.component';
import { FooterComponent } from './components/footer/footer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResultsComponent } from './components/results/results.component';
import { HttpClientModule } from '@angular/common/http';
import { DBConfig, NgxIndexedDBModule } from 'ngx-indexed-db';
import { SpinnerComponent } from './utilities/spinner/spinner.component';
import { DialogComponent } from './utilities/dialog/dialog.component';
import { ResultsScratchboxComponent } from './components/results-scratchbox/results-scratchbox.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { ItemComponent } from './components/item/item.component';
import { SnackbarComponent } from './components/snackbar/snackbar.component';

const dbConfig: DBConfig = {
  name: 'DomainsDb',
  version: 1,
  objectStoresMeta: [
    {
      store: 'domains',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        {
          name: 'domain',
          keypath: 'domain',
          options: { unique: false },
        },
      ],
    },
  ],
};

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    HintComponent,
    WordFormsComponent,
    HelpTextComponent,
    CombineButtonComponent,
    ScratchboxComponent,
    CheckoneFormComponent,
    FooterComponent,
    ResultsComponent,
    SpinnerComponent,
    DialogComponent,
    ResultsScratchboxComponent,
    ListItemComponent,
    ItemComponent,
    SnackbarComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgxIndexedDBModule.forRoot(dbConfig),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
